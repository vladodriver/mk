/*global MK*/
(function() {
  "use strict";
  
  MK.Io = {
    /*Spouští validaci parsovaného objektu a když vrátí true volá input funkci a vrací objekt pro parser*/
    save: function save(specob) {
      if(typeof specob === 'object' && specob.valid) {
        //validuje a zavolá io funkci input pro daný tyo dat a vloží výsledek do parsedValue
        specob.parsedValue = MK.Io[specob.ioname].input(specob);
      }
    },
    /*Načte parsovanou hodnotu, když se změní a účastní se výpočtu*/
    load: function load(specob) {
      if(typeof specob === 'object' && specob.valid) {
        //validuje a načte výsledek do value a do domel pres MK.Io[specob.ioname].output
        specob.domel.innerHTML = specob.value = MK.Io[specob.ioname].output(specob);
      }
    },
    
    /*Každý editovatelný a validovatelný  má svoji metodu 
    input(převod do objektu dat) a 
    output (převod nazpět do stringu pro DOM) metodu*/
    
    /*Účetní období výpisu z "Za období: mm - yyyy" na objekt Date() start a end*/
    acmonth: {
      input: function input(specob) {
        var r = MK.getSpec().Formats.acmonth.reg;
        var my = r.exec(specob.value);
        var m = parseInt(my[1], 10);
        var y = parseInt(my[2], 10);

        var start = new Date(y, m -1, 1, 0, 0, 0, 0);
        var end = new Date(y, m, 0, 23, 59, 59, 999);
        return {start: start, end: end};
      },
      /*uložení do DOM*/
      output: function output(specob) {
        var rangeob = specob.parsedValue;
        var m = (rangeob.start.getMonth() + 1).toString();
        var y = rangeob.start.getFullYear().toString();
        MK.Util.fillZero(m, 2); //doplnit nulu do cisla mesiceje li treba
        var val = 'Za období: ' + m + ' - ' + y;
        return val;
      }
    },
    
    /*Účetní období výpisu typu "Za období: dd.mm.yyyy - dd.mm.yyyy" na objekt Date() start a end*/
    acrange: {
      input: function input(specob) {
        var r = MK.getSpec().Formats.acrange.reg; //nema $ (vyjimka kvuli 'Vypisu za zvolené období')
        var mr = r.exec(specob.value);
        var sday = parseInt(mr[1], 10);
        var smon = parseInt(mr[2], 10);
        var syear = parseInt(mr[3], 10);
        var eday = parseInt(mr[4], 10);
        var emon = parseInt(mr[5], 10);
        var eyear = parseInt(mr[6], 10);
        
        //create start and end Date() object
        var start = new Date(syear, smon - 1, sday, 0, 0, 0, 0);
        var end = new Date(eyear, emon - 1, eday, 23, 59, 59, 999);
        return {start: start, end: end};
      },
      output: function output(specob) {
        var raob = specob.parsedValue;
        var sd = raob.start.getDate().toString();
        var sm = (raob.start.getMonth() + 1).toString();
        var sy = raob.start.getFullYear().toString();
        var ed = raob.end.getDate().toString();
        var em = (raob.end.getMonth() + 1).toString();
        var ey = raob.end.getFullYear().toString();
        
        //doplnit nulu do cisla dne a mesice je - li treba
        sd = MK.Util.fillZero(sd, 2); ed = MK.Util.fillZero(ed, 2);
        sm = MK.Util.fillZero(sm, 2); em = MK.Util.fillZero(em, 2);
        
        var val = 'Za období:\n    												' +
          sd + '.' + sm + '.' + sy + ' -\n    												' +
          ed + '.' + em + '.' + ey;
        
        var replacer = function() {return val;};
        var reg = MK.getSpec().Formats.acrange.reg;
        var out = specob.value.replace(reg, replacer);
        return out;
      }
    },
    
    acflow: {
      /*Konverze "...dd.ddd.ddd,dd" na float a zpět*/
      input: function input(specob) {
        var value = specob.value;
        var reg = new RegExp('\\' + MK.thSep, 'g');
        var kc = parseFloat(value.replace(reg, '').replace(',', '.'));
        return kc;
      },
      output: function output(specob) {
        var num = specob.parsedValue;
        var kcstr = MK.Util.round(num).toString().split('.');
        if (kcstr.length === 1) {
          kcstr.push('00'); //doplni 2 nuly v pripade celeho cisla
        } else if (kcstr[1].length === 1) {
          kcstr[1] += '0';
        }
        kcstr[0] = kcstr[0].replace(/\B(?=(\d{3})+($))/g, MK.thSep); //pridá oddělovač 1000
        return kcstr.join(',');
      }
    },
    
    ackcnum: {
      /*Konverze "...dd(.| )ddd.ddd,dd (Kč|CZK)" na float a zpět*/
      input: function input(specob) {
        var value = specob.value;
        var reg = new RegExp('(\\' + MK.thSep + ')|(\\xa0| )(' + MK.curSym + ')', 'g');
        var kc = parseFloat(value.replace(reg, '').replace(',', '.'));
        return kc;
      },
      output: function output(specob) {
        var num = specob.parsedValue;
        var out = MK.Io.acflow.output(specob) + ' ' + MK.curSym;
        //pro typ vypisu acrange se (-) nezobrazuje a do vystupu jde absolutni hodnota
        if(specob === MK.getSpec().statik['Debetní položky']['Částka'] && MK.stType === 'acrange') {
          out = out.replace('-', '');
        }
        return out;
      }
    },
    
    /*Pouze string číslo na init a zpět*/
    number: {
      input: function input(specob) {
        var value = specob.value;
    	  return parseInt(value, 10);
      },
      output: function output(specob) {
        var num = specob.parsedValue;
        return num.toString();
      }
    },
    
    text: {
      /*vstup i výstup se neupravuje*/
      input: function input(specob) {
				//console.log('INPUT RAW', value);
    	  var value = specob.value;
    	  var out = MK.Util.etRepl({
  				'<': '&lt;',
  				'>': '&gt;',
  				'"': '&quot;',
  				"'" : '&lsquo;'
				}, value);
				//console.log('INPUT FILTERED', out);
				return out;
      }
    },
    
    /*Pro datum ve formátu dd.mm.yyyy*/
    truedate: {
      input: function input(specob) {
        var value = specob.value;
        var dmy = value.split('.');
        var d = parseInt(dmy[0], 10);
        var m = parseInt(dmy[1], 10) - 1;
        var y = parseInt(dmy[2], 10);
        return new Date(y, m, d);
      },
      output: function output(specob) {
        var dateob = specob.parsedValue;
        var d = dateob.getDate().toString();
        var m = (dateob.getMonth() + 1).toString();
        var y = dateob.getFullYear().toString();
        d = (d.length === 1) ? '0' + d : d;
        m = (m.length === 1) ? '0' + m : m;
        return d + '.' + m + '.' + y;
      }
    }
  };
  //Io.text both is same
  MK.Io.text.output = MK.Io.text.input;
})();