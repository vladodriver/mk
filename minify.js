#!/usr/bin/env node
"use strict";
var fs = require('fs');
var ugjs = require('uglify-js');
var ugcss = require('uglifycss');

//nnEmptyArray test
function isNoEmptyArray(ar) {
  return (typeof ar === 'object' &&
    ar.constructor.name === 'Array' && ar.length > 0);
}

//minify html string
function mhtml_string(str) {
  return str.toString()
    .replace(/\s{2,}/g, '')
    .replace(/\'/g, '"')
    .replace(/[\n\r\t]/, '');
}

//get html parts
function getHtmlParts(htmlf, version) {
  var html = fs.readFileSync(htmlf).toString();
  html = html.replace(/(Kouzelná banka)/mg, '$1 ' + version);
  var splt = html.match(
    /^([\s\S]+)(<link rel="stylesheet"[\s\S]+<\/script>)([\s\S]+)$/m
  );
  return { 
    start: mhtml_string(splt[1]),
    end: mhtml_string(splt[3])
  };
}

/**Minify functions**/
//css file min
function minCss(cssf) {
  return ugcss.processFiles([cssf]);
}

//js files min
function minJs(files) {
  var out = '';
  if(isNoEmptyArray(files)) {
    files.forEach(file => {
      let fstr = fs.readFileSync(file).toString();
      out += ugjs.minify(fstr).code
    });
  }
  return out;
}

//make main.js minified part
function getMainJs(jsFile) {
  var jsf = fs.readFileSync(jsFile).toString();
  var reg = /loader\(modules.+function main.+\{([\s\S]+)\}\);/m;
  var mjs = jsf.match(reg)[1];
  mjs = 
'var MK=(typeof MK === "object") ? MK : {};' +
  'window.onload = function() {' +
  mjs +
'};';
  return ugjs.minify(mjs).code;
}

//MINIFY//
console.log('-> Get version from package.json');
var version = JSON.parse(fs.readFileSync('package.json').toString()).version;
console.log('-> Start make minified html5 app mk-editor version: ' + version);
console.log('-> Get html parts from MK.html code..');
var html_parts = getHtmlParts('MK.html', version);
var out = '';

out += html_parts.start; //start html
out += '<style type="text/css">';
console.log('-> Minify and add css/mk.css file..');
out += minCss('css/mk.css'); //app css
console.log('-> End style tag and open script tag, start js parts...');
out += "</style><script>";
console.log('-> Add main.js code - minified version..');
out += getMainJs('js/main.js');
console.log('-> Add minified iframe.css as MK.ifrcss property..');
var ifrcss = minCss('css/iframe.css');
out += "MK.ifrcss='" + ifrcss + "';";
console.log('-> Minify all mk-* js files..');
var jsdir = fs.readdirSync('js');
jsdir = jsdir.filter(function(e) {
  return e !== 'main.js';
});
jsdir = jsdir.map(function(e) {
  return 'js/' + e;
});
console.log(jsdir.join(', '));
out += minJs(jsdir); //all js files
console.log('-> End script tag and add html end part..');
out += '</script>';
out += html_parts.end;
console.log('-> Write all work to MK-min.html');
//WRITE TO FILE
fs.writeFileSync('MK-min.html', out);
console.log('-> Well done, file MK-min.html is ready :)');