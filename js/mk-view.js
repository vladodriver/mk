(function() {
  "use strict";
  /******* Viewer *************************************************************/
  /*global MK*/
  MK.View = function View(parser, editor) {
    this.parser = parser; //Instance parseru
    this.editor = editor; //Instance editoru
    this.parser.view = this; //instance view pro parser
    this.editor.view = this; //instance view pro editor
    
    this.loadedFile = null; //objekt aktuálně načteného souboru pro reader
    this.reader = new FileReader(); //pro načtení souboru z inputu
    
    /*Nody*/
    this.msgs = document.querySelector('#msgs'); //logy aplikace
    this.logmsg = document.querySelector('#logmsg'); //logy elementů
    this.logerr = document.querySelector('#logerr'); //logy elementů
    this.editorel = document.querySelector('#editor'); //element editoru
    this.grab = document.querySelector('#grabarea'); //element editoru
    this.loader = document.querySelector('#loader'); //načítá soubor - skrytý
    this.appform = document.querySelector('#app-form'); //formulář s zákl. tlačítky app.
    this.loadfile = document.querySelector('#loadfile'); //otevírá výběr souboru pro loader
    this.editmod = document.querySelector('#editmod'); //tlačítko zapnutí/vypnutí editace
    this.saveb = document.querySelector('#save'); //tlačítko uložení souboru
    this.printb = document.querySelector('#print'); //tlačítko tisku
    this.edmode = document.querySelector('#editing-mode'); //select přepnutí mode editingu
    this.filename = document.querySelector('#filename'); //element pro zobr. názvu souboru
    this.recalc = document.querySelector('#recalculate'); //tlačítko OK na update/přepočet
    this.acts = document.querySelectorAll('.act'); //schovávatelná tlačítka
    
    /*zapnutí manažera resize app okna a nahrani appLoad*/
    this.appStart();
  };
  
  MK.View.prototype.appStart = function appStart() {
    /*paramery pozice myši + rozměry na resize*/
    this.ifr = document.querySelector('#statement'); //iframe obsah
    this.app = document.querySelector('#app'); //okno applikace vlevo
    this.mpos = 0;
    this.clickpos = 0;
    this.oldwidth = 0;
    this.resized = false;
    var self = this;
    
    this.appLoad();

    var resize = function resize(delay) {
      if(self.resized && self.clickpos && self.oldwidth) {
        setTimeout(function() {
          var dif = self.clickpos - self.mpos;
          var newwidth = self.oldwidth + dif;
          self.app.style.width = (newwidth - 16).toString() + 'px'; // - padding 1em !!
          resize(delay); //recursion on delay
        }, delay);
      }
    };
    
    this.app.onmouseup = this.ifr.contentDocument.documentElement.onmouseup = function resizeStop(e) {
      e.preventDefault();
      self.resized = false;
    };
    this.grab.onmousedown = function appGrab(e) {
      e.preventDefault();
      self.resized = true;
      self.clickpos = e.pageX;
      self.oldwidth = self.app.clientWidth - 16; // - padding 1em !!
      resize(60);
    };
    this.app.onmousemove = this.ifr.contentDocument.documentElement.onmousemove = function appResize(e) {
      e.preventDefault();
      self.mpos = e.pageX;
    };
  };
  
  MK.View.prototype.trueUTF = function trueUTF(s) {
    /*Detect undecoded chars \uFFFD to valid UTF8*/
    var ar = s.split('\uFFFD');
    return (ar.length === 1);
  };
  
  MK.View.prototype.appLoad = function appLoad() {
    var self = this;
    
    this.loadfile.onclick = function loadApp(e) {
      e.preventDefault();
      self.loader.click();
    };
    this.loader.onchange = function loadFile() {
      self.enc = null; //reset detekce encodingu
      if(self.loader.files[0]) {
        var selfname = location.pathname.match(/[^\/]+?$/)[0];
        if (self.loader.files[0].name === selfname) {
          self.clearEls('logerr');
          self.renderLog({type: 'err', text: 'Nemůžete načíst vlastní soubor aplikace: <b>' + selfname + '</b> do sebe sama!'});
          self.appform.reset();
          return false;
        }
        self.loadedFile = self.loader.files[0];
        self.reader.readAsText(self.loadedFile);
      }
    };
    this.reader.onerror = function loadFileErr() {
      self.deactive(true); //pri chybe deaktivovat
      self.ifr.contentDocument.documentElement.innerHTML = null;
      self.clearEls('logerr');
      self.renderLog({type: 'err', text: '<b>Chyba při načtení souboru: </b>' + this.error.message});
      self.renderLog({type: 'msg', text: 'Editor je připraven. Tlačítkem <b>Otevřít</b> otevřete html soubor s výpisem.'});
    };
    this.reader.onload = function fileLoaded() {
      if (self.trueUTF(this.result)) {
        //utf8 result nalit do iframu
        self.fileToIframe(this.result);
      } else {
        //načíst jako 'iso-8859-2' a znovu vyvolat inload
        this.readAsText(self.loadedFile, 'iso-8859-2');
      }
    };

    this.editmod.onclick = function editModeSwitch() {
      self.editMode();
    };
    
    this.saveb.onclick = function fileLoading() {
      self.save(self.loadedFile.name);
    };

    this.printb.onclick = function filePrinting() {
      self.print();
    };

    this.edmode.onchange = function editModeChange() {
      var mode = this.options[this.selectedIndex].value;
      self.editor.changeEdmode(mode);
    };

    this.renderLog({type: 'msg', text: 'Editor je připraven. Tlačítkem <b>Soubor</b> otevřete html soubor s výpisem.'});
    this.deactive(true); //vypnout tlacitka
  };
  
  MK.View.prototype.fileToIframe = function fileToIframe(str) {
    MK.resetSpec();
    this.parser.errors = 0;
    this.parser.log = [];
  
    this.ifr.contentDocument.documentElement.innerHTML = str;
    this.setUtfCharset(); //preznačkovat charset na utf8
    this.filename.innerHTML = '<b>Načtený soubor:</b> ' + this.loadedFile.name;
    
    var ifrDoc = this.ifr.contentDocument;
    //logika rozhodnutí o typu výpisu (měsíční || transakční historie)
    if(ifrDoc.title === 'Výpis z účtu') {
      MK.stType = 'acmonth';
      MK.curSym = 'Kč';
      MK.thSep = '.';
      MK.thSepName = 'tečka';
    } else if(ifrDoc.title === 'Transakční historie') {
      MK.stType = 'acrange';
      MK.curSym = 'CZK';
      MK.thSep = ' ';
      MK.thSepName = 'mezera';
    }
    
    this.editor.editOff();
    this.deactive(false);
    this.clearEls('logerr');
  };
  
  MK.View.prototype.deactive = function deactivate(bol) {
    /*vypnutí ostatních tlačítek než se načte soubor*/
    for (var i = 0; i < this.acts.length; i++) {
      this.acts[i].disabled = bol;
    }
  };
  
  MK.View.prototype.loadcss = function loadcss(url) {
    var css; /*css link or script element for minified version*/
    if (typeof MK.ifrcss === 'string') { /*For inject in minified one file version*/
      css = document.createElement('style');
      css.type = 'text/css';
      css.id = 'editor-css';
      css.innerHTML = MK.ifrcss;
    } else { /*For develop version*/
      css = document.createElement('link');
      css.id = 'editor-css';
      css.rel = 'stylesheet';
      css.href = url;
    }
    this.ifr.contentDocument.head.appendChild(css);
  };
  
  MK.View.prototype.unloadcss = function unloadcss() {
    var css = this.ifr.contentDocument.head.querySelector('#editor-css');
    if (css) {
      css.parentElement.removeChild(css);
    }
  };
  
  MK.View.prototype.setUtfCharset = function setUtfCharset() {
    var meta = this.ifr.contentDocument.querySelector('meta[content]');
    if (meta) {
      var utfmeta = 'text/html; charset=utf-8';
      var enc = meta.getAttribute('content');
      if(enc !== utfmeta) {
        meta.setAttribute('content', utfmeta);
      }
    }
  };
  
  MK.View.prototype.renderOneResult = function renderOneResult(spec, domel) {
    /*Obarví výsledky přiřadí editační akce atd*/
    var self = this;
    var valid = spec.valid;
    var perms = spec.perms;
    if (valid && (perms === 'write')) {
      domel.setAttribute('data-status', 'write');
    } else if (valid && (perms === 'read')) {
      domel.setAttribute('data-status', 'read');
    } else if (!valid) {
      domel.setAttribute('data-status', 'error');
    }
    
    domel.onmouseover = function() {
      self.renderElementLog(spec, 'add');
    };
    domel.onmouseout = function() {
      self.renderElementLog(spec, 'del');
    };
  };
  
  MK.View.prototype.msgDomErr = function msgDomErr() {
    /*Vizuální reakce na chyby během parsování*/
    if (this.parser.errors > 0) {
      this.renderLog({type:'err', text: 'Bylo nalezenno <b>' + this.parser.errors +
        '</b> chyb! Je li načten HTML dokument výpisu, jsou chyby označeny červeně a je možno je opravit v režimu <b>Editace dat transakcí</b>. Výpočet transakcí nebude proveden!'});
    }
  };
  
  MK.View.prototype.clearEls = function clearEls(ids) {
    if(typeof ids === 'object' && Array.isArray(ids)) {
      for (var i = 0; i < ids.length; i++) {
        this.clearEls(ids[i]);
      }
    } else if (typeof ids === 'string') {
      var el = document.querySelector('#' + ids);
      el.innerHTML = '';
      el.style.display = 'none';
    }
  };
  
  /* Loguje chybu nebo msg do app logu*/
  MK.View.prototype.renderLog = function renderLog(logob) {
    var html = '';
    var gethtml = function(onemsg) {
      return '<p class="' + onemsg.type + '">' + onemsg.text + '</p>';
    };
    /*Get html pro pole log zpráv nebo jednu msg*/
    if (typeof logob === 'object' && logob.length) {
      for (var i = 0; i < logob.length; i++) {
        html += gethtml(logob[i]);
      }
    } else if (typeof logob === 'object' && logob.text && logob.type) {
      html = gethtml(logob);
    }
    if (logob.type === 'err') {
      this.logerr.style.display = 'block';
      this.logerr.innerHTML += html;
    } else if (logob.type === 'msg') {
      this.logmsg.style.display = 'block';
      this.logmsg.innerHTML = html;
    }
  };
  
  /*vyrobí bublinu na najetí myší s logem výsledků*/
  MK.View.prototype.renderElementLog = function renderElementLog(spec, action) {
    var logel = document.querySelector('#msgs');
    if (action === 'del') {
      logel.innerHTML = this.editor.emodes[this.editor.editingmode].msg;
    } else if (action === 'add') {
      var buble = '';
      for (var i = 0; i < spec.log.length; i++) {
        var logob = spec.log[i];
        buble += '<p class="' + logob.type + '">' + logob.text + '</p>';
      }
      logel.style.display = 'block';
      logel.innerHTML = buble;
    }
  };
  
  /*Funkce na tlačítka*/
  MK.View.prototype.editMode = function editMode() {
    if (this.editor.editing) {
      this.editor.editOff();
    } else {
      this.editor.editOn();
    }
  };
  
  MK.View.prototype.save = function save() {
    //vypnout editaci když je zapnutá
    var editedBefore = false;
    if (this.editor.editing) {
      editedBefore = true;
      this.editor.editOff();
    }
    
    /*získej data*/
    var text = '<html>\n  ';
    text += this.ifr.contentDocument.documentElement.innerHTML;
    text = text.replace(/^\s*\n/gm, ''); //smaze prazdne linky
    text += '<html>';
    
    if (window.URL && typeof window.URL.createObjectURL === 'function') {
      var blob = new Blob([text], {type: 'text/html'});
      var url = window.URL.createObjectURL(blob);
      var ah = document.createElement('a');
      ah.download = this.loadedFile.name;
      ah.href = url;
      ah.style.visibility = 'hidden';
      ah.style.position = 'absolute';
      ah.style.top = 0;
      document.body.appendChild(ah);
      ah.click();
      ah.parentNode.removeChild(ah);
    } else {
      this.clearEls('logerr');
      this.renderLog({
        type: 'err',
        text: 'Prohlížeč zřejmě nepodporuje vlastnost <a href="https://developer.mozilla.org/en-US/docs/Web/API/URL.createObjectURL">createObjectURL</a> !' +
          'Nezoufejte, alespoň tisk bude určitě fungovat.'
      });
    }
    
    //zapnout editaci zpet jestli byla zapnuta
    if (editedBefore) {
      this.editor.editOn();
    }
  };
  
  MK.View.prototype.print = function print() {
    //uložit rozdělané
    this.editor.update();
    var editedBefore = false;
    //vypnout editaci když je zapnutá
    if (this.editor.editing) {
      editedBefore = true;
      this.editor.editOff();
    }
    var win = window.open('about:blank', '', 'width=680,height=600');
    win.document.write(this.ifr.contentDocument.documentElement.outerHTML);
    win.print();
    win.close();
    //vrátit zpět jak bylo
    if (editedBefore) {
      this.editor.editOn();
    }
  };

})();