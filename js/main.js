"use strict";
var MK = (typeof MK === 'object') ? MK : {};
window.onload = function() {
  //loadstart
  var loader = function loader(ar, cb) {
    
    var end = function(e) {
      var f = ar.shift();
      console.log('Loaded module:', f, e);
      this.parentNode.removeChild(this);
      if (!ar[0]) {
        console.log('All module loading done!');
        cb();
      }
    };

    var err = function err(e) {
      throw new Error('Error when loading module:', e);
    };
    
    ar.forEach(function(e) {
      var s = document.createElement('script');
      s.async = true;
      document.head.appendChild(s);
      s.onerror = err;
      s.onload = end;
      s.src = e;
    });
  };
  
  var modules = [
    'js/mk-editor.js',
    'js/mk-history.js',
    'js/mk-io.js',
    'js/mk-parser.js',
    'js/mk-spec.js',
    'js/mk-util.js',
    'js/mk-validators.js',
    'js/mk-view.js'
  ];
  //loadend
  
  loader(modules, function main() {
    /******* Main Program ********/
    var parser = new MK.Parser();
    var history = new MK.History();
    var editor = new MK.Editor(history);
    new MK.View(parser, editor);
  });
};